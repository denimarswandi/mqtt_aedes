const aedes = require('aedes')()
const server = require('net').createServer(aedes.handle)
const port = 1883



server.listen(port, function () {
  console.log('server started and listening on port ', port)
})

aedes.on('subscribe', function (subscriptions, client) {
  console.log('MQTT client \x1b[32m' + (client ? client.id : client) +
          '\x1b[0m subscribed to topics: ' + subscriptions.map(s => s.topic).join('\n'), 'from broker', aedes.id)
})

aedes.on('publish', async (packet, client)=>{
  console.log(packet.payload.toString())
  //console.log(client.id)
})
